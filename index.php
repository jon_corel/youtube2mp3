<?php echo '<?xml version="1.1" encoding="utf-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>YouTube To Mp3 Converter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/main.css" >
    <script type="text/javascript">
		function startloading() {
			var youtube_url = document.getElementById('youtube_url').value;
			var video_id = youtube_url.split('v=')[1];
			var ampersandPosition = video_id.indexOf('&');
			if(ampersandPosition != -1) {
			  video_id = video_id.substring(0, ampersandPosition);
			}
			var img_url = "http://img.youtube.com/vi/"+ video_id +"/1.jpg"
			
			document.getElementById('preview_img').src = img_url;
			
			var main_form = document.getElementById('main_form').style.display = "none";
			var load_div = document.getElementById('load_div').style.display = "block";
			return true;
		}
    </script>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="index.php">YouTube To Mp3 Converter</a>
        </div>
      </div>
    </div>
	<div class="container">
	<div class="well">
		<div id ="load_div">
			<div id="preview"  class= "" >
				<h3> Converting your video ...</h3>
				<img id= "preview_img" src="" alt="preview image" />
				<center><img id="loading" src="imgs/loading.gif" alt="preview image"></center>
				<div id="video_name"></div>
			</div>
		</div>
    <?php
		session_start();
		
        // Execution settings
        ini_set('max_execution_time',0);
        ini_set('display_errors',0);
        
        // On form submission...
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['youtubeURL'])
        {
            // Instantiate converter class
            include 'Yt2Mp3.class.inc';
            $yt2Mp3 = new Yt2Mp3();            
            
            // Print "please wait" message and preview image
            $vidID = $vidTitle = '';
            $urlQueryStr = parse_url(trim($_POST['youtubeURL']), PHP_URL_QUERY);
            if ($urlQueryStr !== false && !empty($urlQueryStr))
            {
                $kvPairs = explode('&', $urlQueryStr);
                foreach ($kvPairs as $v)
                {
                    $kvPair = explode('=', $v);
                    if ($kvPair[0] == 'v')
                    {
                        $vidID = $kvPair[1];
                        break;
                    }
                }
            }

            // Main Program Execution
            if ($yt2Mp3->DownloadVideo(trim($_POST['youtubeURL'])))
            {
                if ($yt2Mp3->GenerateMP3($_POST['quality']))
                {
					$_SESSION['file_url'] = $yt2Mp3->GetSongFileName();
					header("Location: success.php");
					exit;
				}
				else 
				{
					$_SESSION['error_message'] = 'can not download the file';
					header("Location: failure.php");
					exit;
				}
            }
            else
            {
				if ( $yt2Mp3->GenerateMP3FromYoutubeUrl(trim($_POST['youtubeURL']), $_POST['quality']))
				{
					$_SESSION['file_url'] = $yt2Mp3->GetSongFileName();
					header("Location: success.php");
					exit;
				}
				else 
				{
					$_SESSION['error_message'] = "can't download the file";
					header("Location: failure.php");
					exit;
				}
            }
        } else {
    ?>
    
			<form id="main_form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
					<legend>YouTube To Mp3 Converter</legend>
					<fieldset>
						<label>Enter a valid YouTube.com video URL</label>
						<input id="youtube_url" type="text" name="youtubeURL" class="span9" placeholder="Enter a valid YouTube.com video URL …"/>
						<span class="help-block">i.e. http://www.youtube.com/watch?v=-ZJDNSp1QJA</span>
					
						<label>Choose the audio quality (better quality results in larger files)</label>
						
						<label class="radio">
							<input type="radio" value="64" name="quality" />Low &nbsp;
						</label>
						<label class="radio">
							<input type="radio" value="128" name="quality" checked="checked" />Medium &nbsp; 
						</label>
						<label class="radio">
							<input type="radio" value="320" name="quality" />High</p>
						</label>
						<button type="submit" name="submit" class="btn btn-primary" onclick="return startloading()">Create MP3 File</button>
					</fieldset>
			</form>
		<?php } ?>
		</div>
    </div>
    
</body>
</html>
